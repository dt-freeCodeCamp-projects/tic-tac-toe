const angular = require('angular');
import '@uirouter/angularjs';
import $ from 'jquery';
import { every } from 'lodash';
import '../node_modules/materialize-css/dist/css/materialize.min.css';
import '../node_modules/materialize-css/dist/js/materialize.min.js';
import './css/index.css';

angular.module('tictactoe', ['ui.router'])
  .config(($stateProvider, $urlRouterProvider) => {

    $urlRouterProvider.otherwise('/selectGameMode');

    const selectGameMode = {
      name: 'selectGameMode',
      url: '/selectGameMode',
      template: require('html-loader!./views/selectGameMode.html')
    }

    const selectCharacter = {
      name: 'selectCharacter',
      url: '/selectCharacter',
      template: require('html-loader!./views/selectCharacter.html')
    }

    const gameState = {
      name: 'game',
      url: '/game',
      template: require('html-loader!./views/game.html')
    }

    $stateProvider.state(selectGameMode);
    $stateProvider.state(selectCharacter);
    $stateProvider.state(gameState);

  })
  .run(($rootScope, $state, $stateParams) => {
  	// assigning $state and $stateParams to $rootScope,
  	// this way we can reference them in templates
  	window._$state = $rootScope.$state = $state;
  	window._$stateParams = $rootScope.$stateParams = $stateParams;
  	window._$rootScope = $rootScope;
  })
  .controller('TicTacToeCtrl', ($scope, $state) => {
    
    const ctrl = this;
    
    $scope.$on("$stateChangeSuccess", (event, toState, toParams, fromState, fromParams) => {
      if (angular.isDefined(toState.data.pageTitle)) {
        $scope.pageTitle = toState.data.pageTitle;
  		}
    });
    
    $scope.$on("$locationChangeStart", (event, next, current) => { 
      if(next === current && next.slice(-5) === '/game' || next === current && next.slice(-16) === '/selectCharacter'){
        event.preventDefault();
        $state.go('selectGameMode');
      }
    });

    $scope.selectGameMode = (playerCount) => {
      if (playerCount === 1) {
        $scope.selectedGameMode = "singleplayer"
      } else if (playerCount === 2) {
        $scope.selectedGameMode = "multiplayer"
      }
    }
    
    $scope.selectCharacter = (playerCharacter) => {
      if ($scope.selectedGameMode === "singleplayer") {
        $scope.p1SelectedCharacter = playerCharacter;
        $scope.computer = playerCharacter === "x" ? "o" : "x";
        $scope.p1Points = 0;
        $scope.cPoints = 0;
        $scope.ties = 0;        
      } else if ($scope.selectedGameMode === "multiplayer") {
        $scope.p1SelectedCharacter = playerCharacter;
        $scope.p2SelectedCharacter = playerCharacter === "x" ? "o" : "x";
        $scope.p1Points = 0;
        $scope.p2Points = 0; 
        $scope.ties = 0;              
      }
    }

    $scope.initializeGame = () => {
      if ($scope.selectedGameMode === "singleplayer") {
        Math.random() >= 0.5 ? $scope.turn = $scope.p1SelectedCharacter : $scope.turn = $scope.computer;
      } else if ($scope.selectedGameMode === "multiplayer") {
        Math.random() >= 0.5 ? $scope.turn = $scope.p1SelectedCharacter : $scope.turn = $scope.p2SelectedCharacter;
      }
      $scope.gameTable = ["", "", "", "", "", "", "", "", ""];
      $scope.winner = "";
      $scope.isTie = false;
    }

    $scope.updateGameTable = (location, character) => {
      switch (location) {
        case "topLeft":
          $scope.gameTable[0] = character;
          break;
        case "topMiddle":
          $scope.gameTable[1] = character;
          break;
        case "topRight":
          $scope.gameTable[2] = character;
          break;
        case "middleLeft":
          $scope.gameTable[3] = character;
          break;
        case "middleMiddle":
          $scope.gameTable[4] = character;
          break;
        case "middleRight":
          $scope.gameTable[5] = character;
          break;
        case "bottomLeft":
          $scope.gameTable[6] = character;
          break;
        case "bottomMiddle":
          $scope.gameTable[7] = character;
          break;
        case "bottomRight":
          $scope.gameTable[8] = character;
          break;
      }
    }

    $scope.updateGameState = (location, character, value) => {
      if (value === "" && $scope.winner === "") {
        if (character === "x") {
          $scope.updateGameTable(location, character);
          $scope.turn = character === "x" ? "o" : "x";
        } else if (character === "o") {
          $scope.updateGameTable(location, character);
          $scope.turn = character === "x" ? "o" : "x";
        }
        $scope.checkWinner(character);
      }
    }

    $scope.checkWinner = character => {
      if($scope.gameTable[1] !== "" && $scope.gameTable[0] === $scope.gameTable[1] && $scope.gameTable[1] === $scope.gameTable[2]) {
        $scope.winner = character;
        $scope.updateCounters(character);
      } else if ($scope.gameTable[4] !== "" && $scope.gameTable[3] === $scope.gameTable[4] && $scope.gameTable[4] === $scope.gameTable[5]) {
        $scope.winner = character;
        $scope.updateCounters(character);
      } else if ($scope.gameTable[7] !== "" && $scope.gameTable[6] === $scope.gameTable[7] && $scope.gameTable[7] === $scope.gameTable[8]) {
        $scope.winner = character;
        $scope.updateCounters(character);
      } else if ($scope.gameTable[3] !== "" && $scope.gameTable[0] === $scope.gameTable[3] && $scope.gameTable[3] === $scope.gameTable[6]) {
        $scope.winner = character;
        $scope.updateCounters(character);
      } else if ($scope.gameTable[4] !== "" && $scope.gameTable[1] === $scope.gameTable[4] && $scope.gameTable[4] === $scope.gameTable[7]) {
        $scope.winner = character;
        $scope.updateCounters(character);
      } else if ($scope.gameTable[5] !== "" && $scope.gameTable[2] === $scope.gameTable[5] && $scope.gameTable[5] === $scope.gameTable[8]) {
        $scope.winner = character;
        $scope.updateCounters(character);
      } else if ($scope.gameTable[4] !== "" && $scope.gameTable[0] === $scope.gameTable[4] && $scope.gameTable[4] === $scope.gameTable[8]) {
        $scope.winner = character;
        $scope.updateCounters(character);
      } else if ($scope.gameTable[4] !== "" && $scope.gameTable[2] === $scope.gameTable[4] && $scope.gameTable[4] === $scope.gameTable[6]) {
        $scope.winner = character;
        $scope.updateCounters(character);
      } else if (every($scope.gameTable, place => place !== "")) {
        $scope.updateCounters("tie");
        $scope.isTie = true;
      }
    }

    $scope.updateCounters = character => {
      if ($scope.selectedGameMode === "singleplayer") {
        if (character === $scope.p1SelectedCharacter) {
          $scope.p1Points++;
        } else if (character === $scope.computer) {
          $scope.cPoints++;
        } else if (character === "tie") {
          $scope.ties++;
        }
      } else if ($scope.selectedGameMode === "multiplayer") {
        if (character === $scope.p1SelectedCharacter) {
          $scope.p1Points++;
        } else if (character === $scope.p2SelectedCharacter) {
          $scope.p2Points++;
        } else if (character === "tie") {
          $scope.ties++;
        }
      }
    }

    $scope.newGame = () => {
      $scope.initializeGame();
    }

  });
